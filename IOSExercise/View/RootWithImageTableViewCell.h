//
//  RootView.h
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootTableViewCell.h"

// Table view cell with UILabel and UITextView, 80dp width of UIImageView
@interface RootWithImageTableViewCell : RootTableViewCell
@end
