//
//  RootView.m
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import "RootTableViewCell.h"

@interface RootTableViewCell(){
 @private
  int _padding;
  BOOL _hideIfAllNull;  // If you want to show the all null json data, you may set it to be false 
}
@end

@implementation RootTableViewCell
@synthesize title = _title, detail = _detail, thumbnail = _thumbnail, thumbnailDimension = _thumbnailDimension;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
  if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
    _hideIfAllNull = true;
    _padding = 10;
    _thumbnailDimension = 0;
    _title = [[UILabel alloc] init];
    [self.contentView addSubview:_title];
    _detail = [[UITextView alloc] init];
    [self.contentView addSubview:_detail];
    _thumbnail = [[UIImageView alloc] init];
    [self.contentView addSubview:_thumbnail];
    [self subviewConfig];
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
  }
  return self;
}

- (CGFloat)getCellHeight{
  if(_thumbnailDimension==0&&[_detail.text isEqualToString:@""]&&[_title.text isEqualToString:@""]&&_hideIfAllNull){
    return 0;
  }
  CGFloat accessoryWidth = 34;
  CGFloat contentWidth = DEVICE_WIDTH - accessoryWidth;
  CGFloat paddings = _padding;
  if(![_title.text isEqualToString:@""]){
    paddings = paddings + _padding;
  }
  if(![_detail.text isEqualToString:@""]||_thumbnailDimension > 0){
    paddings = paddings + _padding;
  }
  CGFloat titleWidth = contentWidth- _padding * 2;
  CGFloat detailWidth = contentWidth - _padding * 2;
  if(_thumbnailDimension>0){
    detailWidth = contentWidth - _padding * 3 - _thumbnailDimension;
  }
  CGSize titleSize = [_title systemLayoutSizeFittingSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
  CGSize detailSize = [_detail systemLayoutSizeFittingSize:CGSizeMake(detailWidth, CGFLOAT_MAX)];
  CGFloat totalHeight = MAX(titleSize.height+detailSize.height,titleSize.height +_thumbnailDimension);
  return totalHeight + paddings + 1;
}

- (void)updateConstraints{
  NSDictionary *metrics = @{
    @"padding":[NSNumber numberWithInt:_padding],
    @"thumbDimesnion":[NSNumber numberWithInt:_thumbnailDimension]
  };
  [self.contentView addConstraints:[NSLayoutConstraint
                                  constraintsWithVisualFormat:@"H:|-padding-[_title]-padding-|"
                                  options:0
                                  metrics:metrics
                                  views:NSDictionaryOfVariableBindings(_title)]];
  [self.contentView addConstraints:[NSLayoutConstraint
                                  constraintsWithVisualFormat:@"V:|-padding-[_title]-padding-[_detail]-|"
                                  options:0
                                  metrics:metrics
                                  views:NSDictionaryOfVariableBindings(_title,_detail)]];
  [self.contentView addConstraints:[NSLayoutConstraint
                                  constraintsWithVisualFormat:@"V:[_title]-padding-[_thumbnail(thumbDimesnion)]"
                                  options:0
                                  metrics:metrics
                                  views:NSDictionaryOfVariableBindings(_title,_thumbnail)]];
  [self.contentView addConstraints:[NSLayoutConstraint
                                  constraintsWithVisualFormat:@"H:|-padding-[_detail]-padding-[_thumbnail(thumbDimesnion)]-padding-|"
                                  options:0
                                  metrics:metrics
                                  views:NSDictionaryOfVariableBindings(_detail,_thumbnail)]];
  [super updateConstraints];
}

- (void)subviewConfig{
  // setup |title| UILabel
  _title.clipsToBounds = YES;
  _title.textColor = [UIColor colorWithRed:46.0/255 green:75.0/255 blue:131.0/255 alpha:1];
  _title.numberOfLines = 0;
  _title.textAlignment = NSTextAlignmentLeft;
  _title.translatesAutoresizingMaskIntoConstraints = NO;
  _title.font = [UIFont fontWithName:@"Times New Roman" size:23];
  [_title setBackgroundColor:[UIColor clearColor]];
  // setup |detail| UITextView
  _detail.clipsToBounds = YES;
  _detail.textColor = [UIColor blackColor];
  _detail.editable = false;
  _detail.selectable = false;
  _detail.textContainer.lineFragmentPadding = 0;
  _detail.textContainerInset = UIEdgeInsetsZero;
  _detail.scrollEnabled = false;
  _detail.textAlignment = NSTextAlignmentLeft;
  _detail.translatesAutoresizingMaskIntoConstraints = NO;
  _detail.font = [UIFont fontWithName:@"Arial" size:16];
  [_title setBackgroundColor:[UIColor clearColor]];
  // setup |thumbnail| UIImageView
  _thumbnail.translatesAutoresizingMaskIntoConstraints = NO;
  _thumbnail.contentMode = UIViewContentModeScaleAspectFit;
  _thumbnail.image = [UIImage imageNamed:TEMP_IMAGE];
}

- (void)dealloc{
  [_title release];
  [_title dealloc];
  [_detail release];
  [_detail dealloc];
  [_thumbnail release];
  [_thumbnail dealloc];
  [super dealloc];
}
@end
