//
//  RootView.m
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import "RootWithImageTableViewCell.h"

@interface RootWithImageTableViewCell()
@end

@implementation RootWithImageTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if(self){
    super.thumbnailDimension = 80;
  }
  return self;
}

@end
