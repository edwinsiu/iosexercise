//
//  RootView.h
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>

// Table view cell with UILabel and UITextView, 0dp width of UIImageView 
@interface RootTableViewCell : UITableViewCell
@property (weak, nonatomic) UILabel *title;
@property (weak, nonatomic) UITextView *detail;
@property (weak, nonatomic) UIImageView *thumbnail;
@property int thumbnailDimension;
- (CGFloat)getCellHeight;
@end
