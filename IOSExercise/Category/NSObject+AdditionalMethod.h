//
//  NSObject+AdditionalMethod.h
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (AdditionalMethod)
-(NSString *) returnStringIfNotNull;
-(Boolean) isNullString;
@end
