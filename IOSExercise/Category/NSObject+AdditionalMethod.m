//
//  NSObject+AdditionalMethod.m
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import "NSObject+AdditionalMethod.h"

@implementation NSObject (AdditionalMethod)
-(NSString *) returnStringIfNotNull{
  if (!self.isNullString) {
    NSString *result = (NSString *)self;
    return result;
  }else{
    return @"";
  }
};

-(Boolean) isNullString{
  if (self != nil && [self isKindOfClass:[NSString class]]) {
    return false;
  }else{
    return true;
  }
};
@end
