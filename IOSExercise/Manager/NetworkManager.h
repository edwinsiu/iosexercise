//
//  ManagerNetwork.h
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// Singlen for handling network connection like get request and download image
@interface NetworkManager : NSObject<NSURLSessionDelegate>
+ (instancetype)sharedInstance;
- (void)getRequestWithUrl:(NSString *)url completion:(void(^)(NSDictionary *))completion;
- (void)downloadImageWithUrl:(NSString *)url completion:(void(^)(UIImage *))completion;
@end
