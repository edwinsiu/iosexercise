//
//  ManagerNetwork.m
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import "NetworkManager.h"

@interface NetworkManager()
@end
@implementation NetworkManager
+ (instancetype)sharedInstance {
  static NetworkManager *sharedInstance = nil;
  static dispatch_once_t onceToken;

  dispatch_once(&onceToken, ^{
    sharedInstance = [[NetworkManager alloc] init];
  });
  return sharedInstance;
}

- (id)init{
  if (self = [super init]) {
    return self;
  }
  return self;
}

- (void)getRequestWithUrl:(NSString *)url completion:(void(^)(NSDictionary *))completion{
  NSURL *nsurl = [NSURL URLWithString:url];
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:nsurl];
  [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler: ^(NSURLResponse * response, NSData * data, NSError * error) {
      NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse*)response;
      if (!error) {
      // Success
        if(httpResponse.statusCode == 200) {
          if (data) {
            NSString *jsonString = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];
            NSData *jsonData = [[jsonString dataUsingEncoding:NSUTF8StringEncoding] autorelease];
            [jsonData retain];
            NSDictionary *dictionary = [[[CJSONDeserializer deserializer] deserializeAsDictionary:jsonData error:&error] autorelease];
            [dictionary retain];
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(dictionary);
            });
          }else{
            completion(@{
              @"error":@"Data format incorrect"
            });
          }
        }else{
          completion(@{
            @"error":@"Web server is returning an error"
          });
        }
      }else{
        completion(@{
          @"error":@"Web server is returning an error"
        });
      }
      [[NSOperationQueue currentQueue] cancelAllOperations];
  }] ;
}

- (void)downloadImageWithUrl:(NSString *)url completion:(void(^)(UIImage *))completion{
  NSURL *nsurl = [NSURL URLWithString:url];
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:nsurl];
  [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler: ^(NSURLResponse * response, NSData * data, NSError * error) {
      NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse*)response;
      if (!error) {
        if(httpResponse.statusCode == 200) {
          if (data) {
            completion([UIImage imageWithData:data]);
          }else{
            completion([UIImage imageNamed:TEMP_IMAGE]);
          }
        }else{
          NSLog(@"%ld+%@",(long)httpResponse.statusCode,url);
          completion([UIImage imageNamed:TEMP_IMAGE]);
        }
      }else{
        NSLog(@"~~%@",error);
        completion([UIImage imageNamed:TEMP_IMAGE]);
      }
  }];
}
@end
