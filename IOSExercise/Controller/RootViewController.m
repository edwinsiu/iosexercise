 //
//  ViewController.m
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController (){
 @private
  NSArray *_rows;
  NSMutableArray *_cachedImages;
  NSMutableArray *_cachedHeights;
  NSMutableArray *_downloadings;
  RootTableViewCell *_modalRootWithImageTableViewCell;
  RootTableViewCell *_modalRootTableViewCell;
}

@end

@implementation RootViewController
- (id)init{
  if(self = [super initWithStyle:UITableViewStylePlain]){
    self.tableView.allowsSelection = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    [self loadContent];
    _rows = @[];
  
    // use for calculate height
    static dispatch_once_t onceToken;
    dispatch_once (&onceToken, ^{
    // Do some work that happens once
      _modalRootTableViewCell = [[RootTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mathRootTableViewCell"];
      _modalRootWithImageTableViewCell = [[RootWithImageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mathRootTableViewCell"];
    });
  }
  return self;
}

// setup refresh control
- (void)loadView{
  [super loadView];
  self.refreshControl = [[UIRefreshControl alloc] init];
  [self.refreshControl addTarget:self action:@selector(loadContent) forControlEvents:UIControlEventValueChanged];
}

// download json file
-(void)loadContent{
  [[NetworkManager sharedInstance] getRequestWithUrl:@"https://dl.dropboxusercontent.com/u/746330/facts.json"   completion:^(NSDictionary *response){
        if([response valueForKey:@"rows"]){
          [_cachedImages autorelease];
          _cachedImages = [[NSMutableArray alloc] init];
          [_downloadings autorelease];
          _downloadings = [[NSMutableArray alloc] init];
          [_cachedHeights autorelease];
          _cachedHeights = [[NSMutableArray alloc] init];
          if(_rows.count>0){
            [_rows release];
          }
          _rows = [[response valueForKey:@"rows"] retain];
          [(AppDelegate *)[UIApplication sharedApplication].delegate setNavigationBarWithTitle:[response valueForKey:@"title"]];
          for (NSInteger i = 0; i < _rows.count; ++i){
            [_cachedImages addObject:[NSNull null]];
          }
          for (NSInteger i = 0; i < _rows.count; ++i){
            [_cachedHeights addObject:[NSNull null]];
          }
          [self.tableView reloadData];
          [self.refreshControl endRefreshing];
          self.tableView.separatorColor = [UIColor lightGrayColor];
        }else{
          // if no content, tap ok to reload again. if pull to refresh, just tap ok to dismiss
          if([self.tableView numberOfRowsInSection:0] == 0){
            UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Server connection error"
                                                             message:@"Tap OK to reload"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil] autorelease];
            [alert retain];
            alert.tag = 0;
            [alert show];
          }else{
            UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Server connection error"
                                                           message:@"Tap OK to dismiss"
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
            [alert retain];
            alert.tag = 1;
            [alert show];
          }
        }
  }];
}

// alertview delegate
- (void)alertView:(UIAlertView *)alert didDismissWithButtonIndex:(NSInteger)buttonIndex{
  if(alert.tag == 0){
    [self loadContent];
  }else{
    [self.refreshControl endRefreshing];
  }
  [alert autorelease];
}

// tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return _rows.count;
}

/* tableview delegate, if imageHerf is null, cell would be _modalRootTableViewCell. If imageHerf
 is not null, cell would be _modalRootWithImageTableViewCell*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
  NSDictionary *row = _rows[indexPath.row];
  NSString *imageHerf = [row valueForKey:@"imageHref"];
  RootTableViewCell *cell = _modalRootWithImageTableViewCell;
  if(imageHerf.isNullString){
    cell = _modalRootTableViewCell;
  }
  [cell.title setText:[[row valueForKey:@"title"] returnStringIfNotNull]];
  [cell.detail setText:[[row valueForKey:@"description"] returnStringIfNotNull]];
  NSNumber *height = _cachedHeights[indexPath.row];
  if([height isKindOfClass:[NSNull class]]){
    _cachedHeights[indexPath.row] = [NSNumber numberWithFloat:[cell getCellHeight]];
  }
  return [_cachedHeights[indexPath.row] floatValue];
}

// tableview delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
  // image config
  NSDictionary *row = _rows[indexPath.row];
  NSString *imageHerf = [row valueForKey:@"imageHref"];
  RootTableViewCell *cell;
  if(imageHerf.isNullString){
    cell = [self.tableView dequeueReusableCellWithIdentifier:@"rootTableViewCell"];
  }else{
    cell = [self.tableView dequeueReusableCellWithIdentifier:@"rootWithImageTableViewCell"];
  }
  if(cell == nil){
    if(imageHerf.isNullString){
      cell = [[RootTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"rootTableViewCell"];
    }else{
      cell = [[RootWithImageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"rootWithImageTableViewCell"];
    }
  }
  // cell set text
  [cell.title setText:[[row valueForKey:@"title"] returnStringIfNotNull]];
  [cell.detail setText:[[row valueForKey:@"description"] returnStringIfNotNull]];
  // download image
  if([_downloadings containsObject:[NSNumber numberWithInteger:indexPath.row]]){
    cell.thumbnail.image = [UIImage imageNamed:TEMP_IMAGE];
  }else if(!imageHerf.isNullString){
    if([_cachedImages[indexPath.row] isKindOfClass:[NSNull class]]){
      [_downloadings addObject:[NSNumber numberWithInteger:indexPath.row]];
      [[NetworkManager sharedInstance] downloadImageWithUrl:imageHerf completion:^(UIImage *image){
          [_downloadings removeObject:[NSNumber numberWithInteger:indexPath.row]];
          _cachedImages[indexPath.row] = image;
          cell.thumbnail.image = image;
      }];
    }else{
      cell.thumbnail.image = _cachedImages[indexPath.row];
    }
  }
  // update constraint
  [cell layoutIfNeeded];
  [cell updateConstraintsIfNeeded];
  return cell;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

- (void)dealloc{
  [_rows release];
  [_rows dealloc];
  [_cachedImages release];
  [_cachedImages dealloc];
  [_downloadings release];
  [_downloadings dealloc];
  [super dealloc];
}

@end
