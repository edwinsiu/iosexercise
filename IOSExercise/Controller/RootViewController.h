//
//  ViewController.h
//  IOSExercise
//
//  Created by User on 9/11/2016.
//  Copyright © 2016 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"
#import "RootTableViewCell.h"
#import "RootWithImageTableViewCell.h"
#import "AppDelegate.h"

// First controller presented by NavigationViewController, which is UIWindow rootViewcontroller
@interface RootViewController : UITableViewController<NSURLSessionDelegate>
@end

